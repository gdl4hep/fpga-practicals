# hls4ml-tutorial: Tutorial notebooks for `hls4ml`

## Setting up hls4ml on the LIP6 server

- Connect to a LIP6 server that has Vivado, e.g. `bip`, `bop`. 

	- From the LIP6 network (use `-X` flag to enable X11 forwarding).

	```
	ssh bop
	```
	
	- From outside.

	```
	ssh -J barder.lip6.fr bop
	```

- If you have not already installed miniforge3, install it.

```
curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
bash Miniforge3-$(uname)-$(uname -m).sh
```

- Source the bashrc file, to initialize miniforge3.

```
source .bashrc
```

- Set up Vivado.

```
source /users/soft/xilinx/2019.1/Vivado/2019.1/settings64.sh
export LM_LICENSE_FILE=27009@house
```

## Installing and activating the environment

To install the chosen environment run the command below.

```
mamba env create -f environment.yaml
```

Activate it.

```
conda activate hls4ml-tutorial
```

## Launch jupyter


- Launch jupyter in the `hls4ml-tutorial` repo.

```
cd /some/path/hls4ml-tutorial/
jupyter notebook
```

- Forward the ports, to access the notebook.

	- From the LIP6 network.

	```
	ssh -L 8888:localhost:8888 bop
	```
	
	- From outside.
	
	```
	ssh -J barder.lip6.fr -L 8888:localhost:8888 bop
	```
	