----------------------------------------------------------------------------------
-- Company: LIP6
-- Engineer: Fotis Giasemis
-- 
-- Create Date: 03/20/2023 01:04:54 PM
-- Design Name: 
-- Module Name: sevenseg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sevenseg is
port (
    value : in std_logic_vector(3 downto 0);
    CA, CB, CC, CD, CE, CF, CG, DP : out std_logic;
    AN0, AN1, AN2, AN3, AN4, AN5, AN6, AN7 : out std_logic
    );
end entity sevenseg;

architecture Behavioral of sevenseg is
signal S : std_logic_vector(7 downto 0);
begin
    S <= "11000000" when value="0000" else
         "11111001" when value="0001" else
         "10100100" when value="0010" else
         "10110000" when value="0011" else
         "10011001" when value="0100" else
         "10010010" when value="0101" else
         "10000010" when value="0110" else
         "11111000" when value="0111" else
         "10000000" when value="1000" else
         "10010000" when value="1001" else
         "11111111" ;
         
CA <= S(0) ;
CB <= S(1) ;  
CC <= S(2) ;  
CD <= S(3) ;  
CE <= S(4) ;  
CF <= S(5) ;  
CG <= S(6) ;  
DP <= S(7) ;

AN0 <= '1' ;
AN1 <= '1' ;
AN2 <= '1' ;
AN3 <= '1' ;
AN4 <= '1' ;
AN5 <= '1' ;
AN6 <= '1' ;
AN7 <= '0' ;
         
         
end Behavioral;


