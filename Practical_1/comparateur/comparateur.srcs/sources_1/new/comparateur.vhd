library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity comparateur is
    Port ( a : in STD_LOGIC_VECTOR (3 downto 0);
           b : in STD_LOGIC_VECTOR (3 downto 0);
           GTin : in STD_LOGIC;
           EQin : in STD_LOGIC;
           LTin : in STD_LOGIC;
           GT : out STD_LOGIC;
           EQ : out STD_LOGIC;
           LT : out STD_LOGIC
);
end comparateur;

architecture flot of comparateur is
signal egal,plusgrand,pluspetit : std_logic_vector(3 downto 0);

begin

   label1: for i in 0 to 3 generate
        egal(i) <= not(a(i) xor b(i));
        plusgrand(i) <= (a(i) and (not(b(i))));
        pluspetit(i) <= (not(a(i)) and b(i));
    end generate label1;
    
EQ <= ((egal(3) and egal(2)) and (egal(1) and egal(0))) and EQin;
GT <= plusgrand(3) or (egal(3) and (plusgrand(2) or (egal(2) and (plusgrand(1) or (egal(1) and (plusgrand(0) or (egal(0) and GTin)))))));
LT <= pluspetit(3) or (egal(3) and (pluspetit(2) or (egal(2) and (pluspetit(1) or (egal(1) and (pluspetit(0) or (egal(0) and LTin)))))));

end flot;
