library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity TestComparateur is
end TestComparateur;

architecture Behavioral of TestComparateur is
signal  a,b : STD_LOGIC_VECTOR (3 downto 0);
signal  GTin, EQin, LTin, GT, EQ, LT :STD_LOGIC;

begin

moncomparateur: entity work.comparateur(flot)
                port map (a => a, b => b, GTin => GTin, EQin => EQin, LTin => LTin, GT => GT, EQ => EQ, LT => LT);

a <= "0000", "0101" after 10ns, "0110" after 20ns, "1110" after 35ns;
b <= "0100", "0110" after 5ns, "1101" after 28ns, "1111" after 40ns;
GTin <= '0';
EQin <= '1';
LTin <= '0';


end Behavioral;
